﻿namespace Kohde.Assessment
{
    //Chris - Inherit from the abstract Animal Base class
    public class Human: Animal
    {
        public Human() { 
        
        }

        public Human(int age, string name) {
            this.Age = age;
            this.Name = name;
        }
        public string Gender { get; set; }

        // Chris - override the abstract method provided by the base class
        public override string GetDetails()
        {
            return "Name: " + Name + "Age: " + Age;
        }


        // Chris - Override the default ToString method of string
        public override string ToString()
        {
            return $"The person is {this.Gender} and their name is {this.Name} and is {this.Age} years old";
            //return base.ToString();
        }

    }
}