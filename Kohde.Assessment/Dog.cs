﻿namespace Kohde.Assessment
{
    //Chris - Inherit from the abstract Animal Base class
    public class Dog: Animal
    {
        public string Food { get; set; }

        public override string GetDetails()
        {
            return "Name: " + Name + "Age: " + Age;
        }
    }
}